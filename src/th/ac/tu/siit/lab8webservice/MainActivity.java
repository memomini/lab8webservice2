package th.ac.tu.siit.lab8webservice;

import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.*;
import org.json.*;
import android.os.*;
import android.app.*;
import android.net.*;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.*;

public class MainActivity extends ListActivity {
	List<Map<String, String>> list;
	SimpleAdapter adapter;
	Long lastUpdate = 0l;
	String current_prov = "bangkok";
	String current_title = "Bangkok";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Do not allow changing device orientation
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		list = new ArrayList<Map<String, String>>();
		
		File infile = getBaseContext().getFileStreamPath("province.dat");
		if (infile.exists()) {
			try {
				BufferedReader reader = new BufferedReader(new FileReader (infile));
				current_prov = reader.readLine().trim();
				current_title = reader.readLine().trim();
				reader.close();
			} catch (Exception e) {
				//Do nothing
				 current_prov = "bangkok";
				 current_title = "Bangkok";
			}
		}
		adapter = new SimpleAdapter(this, list, R.layout.item, 
				new String[] {"name", "value"}, 
				new int[] {R.id.tvName, R.id.tvValue});
		setListAdapter(adapter);
	}

	@Override
	protected void onStart() {
		super.onStart();
		
		refresh(5, "bangkok");
	}

	public void refresh(int call_min, String c) {
		// TODO Auto-generated method stub
		try {
			FileOutputStream outfile = openFileOutput("province.dot", MODE_PRIVATE);
			PrintWriter p = new PrintWriter(outfile);
			p.write(current_prov + "\n");
			p.write(current_title + "\n");

			p.flush();
			p.close();
			outfile.close();
		} catch (FileNotFoundException e) {
			Toast t = Toast.makeText(this, "Error: Unable to save data",
					Toast.LENGTH_SHORT);
			t.show();
		} catch (IOException e) {
			Toast t = Toast.makeText(this, "Error: Unable to save data",
					Toast.LENGTH_SHORT);
			t.show();
		}
		// Check if the device has Internet connection
		ConnectivityManager mgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = mgr.getActiveNetworkInfo();
		if (info != null && info.isConnected()) {
			// Load data when there is a connection
			long current = System.currentTimeMillis();
			if (current - lastUpdate > call_min * 1000) {
				WeatherTask task = new WeatherTask(this);
				task.execute("http://cholwich.org/" + c + ".json");
			}
		} else {
			Toast t = Toast.makeText(this,"No Internet Connectivity on this device. Check your setting",
					Toast.LENGTH_LONG);
			t.show();
		}

	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		case R.id.action_refresh:
			refresh(3, current_prov);
			break;
			
		case R.id.action_bangkok:
			int r = 0;
			if (current_prov == "bangkok")
				r = 5;

			current_prov = "bangkok";
			current_title = "Bangkok";
			refresh(r, current_prov);
			break;

		case R.id.action_nonthaburi:
			int r1 = 0;
			if (current_prov == "nonthaburi")
				r1 = 5;

			current_prov = "nonthaburi";
			current_title = "Nonthaburi";

			refresh(r1, current_prov);
			break;
		case R.id.action_pathumthani:

			int r2 = 0;
			if (current_prov == "pathumthani")
				r2 = 5;

			current_prov = "pathum thani";
			current_title = "Pathum Thani";
			refresh(r2, current_prov);
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	class WeatherTask extends AsyncTask<String, Void, String> {
		Map<String, String> record;
		ProgressDialog dialog;

		public WeatherTask(MainActivity m) {
			dialog = new ProgressDialog(m);
		}

		// Executed under UI thread
		// Called before the task starts
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.setMessage("Loading Weather Data");
			dialog.show();
		}

		// Executed under UI thread
		// Called after the task finished
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (dialog.isShowing()) {
				dialog.dismiss();
			}
			
			//Toast t = Toast.makeText(getApplicationContext(), 
			//		result, Toast.LENGTH_LONG);
			//t.show();
			
			
			adapter.notifyDataSetChanged();
			lastUpdate = System.currentTimeMillis();
			

			Date d = new Date(lastUpdate);
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy - hh:mm:ss");
	        
			Toast t = Toast.makeText(getApplicationContext(), 
					result + "\nThe information has been refresh since : " + dateFormat.format(d) , 
						Toast.LENGTH_LONG);
			t.show();
			
			
			setTitle(current_title+" Weather");
		}

		// Executed under Background thread
		@Override
		protected String doInBackground(String... params) {
			BufferedReader in = null;
			StringBuilder buffer = new StringBuilder();
			String line;
			int response;
			try {
				// Use the first parameter as a URL
				URL url = new URL(params[0]);
				// Connect to the URL
				HttpURLConnection http = (HttpURLConnection) url.openConnection();
				http.setReadTimeout(10000);
				http.setConnectTimeout(15000);
				http.setRequestMethod("GET");
				// Read data from the URL
				http.setDoInput(true);
				http.connect();

				response = http.getResponseCode();
				if (response == 200) {
					in = new BufferedReader(new InputStreamReader(
							http.getInputStream()));
					while ((line = in.readLine()) != null) {
						buffer.append(line);
					}
					JSONObject json = new JSONObject(buffer.toString());
					JSONObject jmain = json.getJSONObject("main");
					list.clear();
					record = new HashMap<String, String>();
					record.put("name", "Temperature");
					double temp = jmain.getDouble("temp") - 273.0;
					record.put("value", String.format(Locale.getDefault(),
							"%.1f degree celsius", temp));
					list.add(record);

					// "weather": [ { "description": ".......", } ],
					JSONArray jweather = json.getJSONArray("weather");
					JSONObject w0 = jweather.getJSONObject(0);
					String desc = w0.getString("description");
					record = new HashMap<String, String>();
					record.put("name", "Description");
					record.put("value", desc);
					list.add(record);

					// pressure
					JSONObject jpressure = json.getJSONObject("main");
					String pres = jpressure.getString("pressure");
					record = new HashMap<String, String>();
					record.put("name", "Pressure");
					record.put("value", pres);
					list.add(record);

					// humidity
					JSONObject jhumidity = json.getJSONObject("main");
					String humi = jhumidity.getString("humidity");
					record = new HashMap<String, String>();
					record.put("name", "Humidity");
					record.put("value", humi);
					list.add(record);

					// temp_min
					JSONObject jtempmin = json.getJSONObject("main");
					String tmin = jtempmin.getString("temp_min");
					record = new HashMap<String, String>();
					record.put("name", "Temp min");
					record.put("value", tmin);
					list.add(record);

					// temp_max
					JSONObject jtempmax = json.getJSONObject("main");
					String tmax = jtempmax.getString("temp_max");
					record = new HashMap<String, String>();
					record.put("name", "Temp max");
					record.put("value", tmax);
					list.add(record);

					// wind speed
					JSONObject jwind = json.getJSONObject("wind");
					String winds = jwind.getString("speed");
					record = new HashMap<String, String>();
					record.put("name", "Wind Speed");
					record.put("value", winds);
					list.add(record);

					// wind degree
					JSONObject jdegree = json.getJSONObject("wind");
					String deg = jdegree.getString("deg");
					record = new HashMap<String, String>();
					record.put("name", "Wind Degree");
					record.put("value", deg);
					list.add(record);

					return "Finished Loading Weather Data";
				} else {
					return "Error " + response;
				}
			} catch (IOException e) {
				return "Error while reading data from the server";
			} catch (JSONException e) {
				return "Error while processing the downloaded data";
			}
		}
	}

}
